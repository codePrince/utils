
import {listToTree} from '../../src/listToTree.js'

let list = [
    {
        id: 'kkk',
        pid: 'r',
        name: '哈哈'
    },
    {
        id: 'sd',
        pid: 'kkk',
        name: '好尴尬'
    },
    {
        id: 'hhh',
        pid: 'sd',
        name: '几个'
    },
    {
        id: 'sss',
        pid: 'vvv',
        name: '来看'
    },
    {
        id: 'ddd',
        pid: 'sss',
        name: '看看'
    },
    {
        id: 'xx',
        pid: 'rrr',
        name: '慢慢'
    },
]

let tree = listToTree({list,fields:{id:'id',pid:'pid'}})
console.log(tree)