
import {RobotJs} from '../../src/index.js'
import $ from 'jquery'

let robotjs = new RobotJs()

function  el_init(params) {
    $('<div>', {class:'box1'}).appendTo('body')
    $(".box1").click(function(){
        console.log('robot test click');
    })
}
$(function(){
    setTimeout(()=>{
        el_init()
    }, 3500)
})

robotjs.wait('.box1').click().perform()