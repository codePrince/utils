
import {throttle} from '../../src/index.js'
import $ from 'jquery'

function test(a,b,c){
    console.log('call me', a,b,c)
}

$(function(){
    var fn = throttle(test, 800)
    $("#testbtn").click(function(){
        fn(1,3,4)
    })
})