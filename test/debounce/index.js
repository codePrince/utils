
import debounce from '../../src/debounce.js'
import $ from 'jquery'

function test(a,b,c){
    console.log('call me', a,b,c)
}

$(function(){
    var fn = debounce(test, 800, false)
    $("#testbtn").click(function(){
        fn(1,3,4)
    })
})