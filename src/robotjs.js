export default class RobotJs {
    constructor() {
        this.selectorType = 'normal'
        this.actions = []
        this.params = {
            el: ''
        }
    }
    wait(selector, {interval = 100, overtime = 3500 } = {}) {
        let fn = ()=>{
            let time = 0
            let el = ''
            if(selector[0] == '/'){
                this.selectorType = 'xpath'
            }else{
                this.selectorType = 'normal'
            }
            let repeater = setInterval(()=>{
                if(this.selectorType == 'xpath'){
                    el = $x(selector)
                    if(el.length!=0){
                        this.params.el = el
                        this.next()
                        clearInterval(repeater)
                    }
                }
                if(this.selectorType == 'normal'){
                    el = document.querySelectorAll(selector)
                    if(el.length!=0){
                        this.params.el = el
                        this.next()
                        clearInterval(repeater)
                    }
                }
                time += interval
            }, interval)
        }
        this.actions.push(fn)
        return this
    }
    next(){
        if(this.actions.length == 0) return
        let ac = this.actions.shift()
        ac()
    }
    perform(){
        this.next()
    }
    click(){
        let fn = ()=>{
            this.params.el.forEach((item)=>{
                item.click()
            })
            this.next()
        }
        this.actions.push(fn)
        return this
    }
    send_keys(str){
        let fn = ()=>{
            let letter = ''
            let strs = str.split(',')
            setInterval(()=>{
                letter = strs.shift()
                this.params.el.val += letter
            }, 250)
        }
        this.actions.push(fn)
        return this
    }
}