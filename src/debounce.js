"use strict";
export default function debounce(fn, delay = 500, immediate) {
    let timerid = null 
    let wait = false
    return function(){
        let args = arguments
        if(immediate){
            if(!wait){
                fn(...args)
                wait = true
            } 
            if(timerid) clearTimeout(timerid) 
            timerid = setTimeout(()=>{
                wait = false
            }, delay)
        }else{
            if(timerid) clearTimeout(timerid)
            timerid = setTimeout(()=>{
                fn(...args)
            }, delay)
        }
    }
}