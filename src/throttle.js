"use strict";
// 节流
export default function throttle(fn, interval = 500){
    let prev = Date.now()
    return function() {
        let args = arguments
        let now = Date.now()
        let past = now - prev
        if(interval < past) {
            fn(...args)
            prev = Date.now()
        }
    }
}