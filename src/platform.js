export const inBrowser = typeof window !== 'undefined'
export const inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform
export const weexPlatform = inWeex && WXEnvironment.platform.toLowerCase()
export const UA = inBrowser && window.navigator.userAgent.toLowerCase()

export function isIE () {
	return UA && /msie|trident/.test(UA)
}
export function isIE9  () {
	return UA && UA.indexOf('msie 9.0') > 0
}
export function isEdge  () {
	return UA && UA.indexOf('edge/') > 0
}
export function isAndroid () {
	return (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android')
}
export function isIOS () {
	return (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios')
}
export function isChrome () {
	return UA && /chrome\/\d+/.test(UA) && !isEdge()
}
export function isPhantomJS () {
	return UA && /phantomjs/.test(UA)
}
export function isFF  () {
	return UA && UA.match(/firefox\/(\d+)/)
}
