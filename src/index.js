import * as _platform from "./platform.js"
import _debounce from "./debounce.js"
import _throttle from "./throttle.js"
import _robotjs from "./robotjs.js"

export const platform =  _platform
export const debounce =  _debounce
export const throttle =  _throttle
export const RobotJs =  _robotjs

// 手机号码脱敏 前三后四 
export function phoneHide(phone){
    if(typeof phone == 'string'){ //参数为字符串类型
        var newPhone = phone.substring(3,8);
        return phone.replace(newPhone,'*****');
    } 
}
 // 深拷贝
export function cloneDeep(data) {
    var t = type(data),
        o, i, ni;
    if (t === 'array') {
        o = [];
    } else if (t === 'object') {
        o = {};
    } else {
        return data;
    }
    if (t === 'array') {
        for (i = 0, ni = data.length; i < ni; i++) {
            o.push(deepCopy(data[i]));
        }
        return o;
    } else if (t === 'object') {
        for (i in data) {
            o[i] = deepCopy(data[i]);
        }
        return o;
    }
}
export function objType(obj) {
  var toString = Object.prototype.toString;
  var map = {
      '[object Boolean]': 'boolean',
      '[object Number]': 'number',
      '[object String]': 'string',
      '[object Function]': 'function',
      '[object Array]': 'array',
      '[object Date]': 'date',
      '[object RegExp]': 'regExp',
      '[object Undefined]': 'undefined',
      '[object Null]': 'null',
      '[object Object]': 'object'
  };
  return map[toString.call(obj)];
}
// 数组去重
export function unique(array) {
    var n = {},
        r = [],
        len = array.length,
        val, type;
    for (var i = 0; i < array.length; i++) {
        val = array[i];
        type = typeof val;
        if (!n[val]) {
            n[val] = [type];
            r.push(val);
        } else if (n[val].indexOf(type) < 0) {
            n[val].push(type);
            r.push(val);
        }
    }
    return r;
}
// 字符串转数字，可以控制精度
export function strToNum(str, num) {
    var num = num ? num : 0;
    var value = parseFloat(str);
    return parseFloat(value.toFixed(num))
}
// 获取视口尺寸
export function getViewportOffset() {
    if (window.innerWidth) {
        return {
            w: window.innerWidth,
            h: window.innerHeight
        }
    } else {
        // ie8及其以下
        if (document.compatMode === "BackCompat") {
            // 怪异模式
            return {
                w: document.body.clientWidth,
                h: document.body.clientHeight
            }
        } else {
            // 标准模式
            return {
                w: document.documentElement.clientWidth,
                h: document.documentElement.clientHeight
            }
        }
    }
}
// 银行卡号分割
export const bank_filter = val =>{
  val += '';
  val = val.replace(/(\s)/g,'').replace(/(\d{4})/g,'$1 ').replace(/\s*$/,'');
  return val;
}
// 查找数组中的元素
export const arrFindItem = function (arr,num) {
  let index = -1;
  for (let i = 0; i < arr.length; i++) {
    if (num == arr[i]) {
      index = i;
      break;
    }
  }
  return index;
}

// 删除数组中的元素
export const delArrItem = (arr,val) => {
  let index = arrFindItem(arr, val)
  if (index != -1) {
    return arr.splice(index, 1);
  }
}
// 统计数组中各个元素出现的次数
export const arrItemCounts = arr => {
  let obj = {};
  for (let i = 0; i < arr.length; i++) {
    let m = arr[i];
    if (obj.hasOwnProperty(m)) {
      obj[m] += 1;
    } else {
      obj[m] = 1;
    }
  }
  return obj;
}
// 快速排序
let times = 0
export const quickSort = arr => {
    let j = arr.length - 1
    let base = null
    let tem = null
    let bIndex = 0
    let i = bIndex
    do {
        base = arr[bIndex]
        do {
            while(j > i) {
                if(arr[j] < base) break;
                j--
            }
            while(j > i ) {
                if( arr[i] > base ) break;
                i++
            }
            if(i != j && j != bIndex) {
                tem = arr[i]
                arr[i] = arr[j]
                arr[j] = tem
            } else if(j != bIndex) {
                tem = arr[i]
                arr[i] = arr[bIndex]
                arr[bIndex] = tem
            } else {
                bIndex++
            }
        } while(i != j)
        i = bIndex
        j = arr.length - 1
    } while(bIndex != j)
}