// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"src/platform.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isIE = isIE;
exports.isIE9 = isIE9;
exports.isEdge = isEdge;
exports.isAndroid = isAndroid;
exports.isIOS = isIOS;
exports.isChrome = isChrome;
exports.isPhantomJS = isPhantomJS;
exports.isFF = isFF;
exports.UA = exports.weexPlatform = exports.inWeex = exports.inBrowser = void 0;
var inBrowser = typeof window !== 'undefined';
exports.inBrowser = inBrowser;
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
exports.inWeex = inWeex;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
exports.weexPlatform = weexPlatform;
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
exports.UA = UA;

function isIE() {
  return UA && /msie|trident/.test(UA);
}

function isIE9() {
  return UA && UA.indexOf('msie 9.0') > 0;
}

function isEdge() {
  return UA && UA.indexOf('edge/') > 0;
}

function isAndroid() {
  return UA && UA.indexOf('android') > 0 || weexPlatform === 'android';
}

function isIOS() {
  return UA && /iphone|ipad|ipod|ios/.test(UA) || weexPlatform === 'ios';
}

function isChrome() {
  return UA && /chrome\/\d+/.test(UA) && !isEdge();
}

function isPhantomJS() {
  return UA && /phantomjs/.test(UA);
}

function isFF() {
  return UA && UA.match(/firefox\/(\d+)/);
}
},{}],"src/debounce.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = debounce;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function debounce(fn) {
  var delay = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
  var immediate = arguments.length > 2 ? arguments[2] : undefined;
  var timerid = null;
  var wait = false;
  return function () {
    var args = arguments;

    if (immediate) {
      if (!wait) {
        fn.apply(void 0, _toConsumableArray(args));
        wait = true;
      }

      if (timerid) clearTimeout(timerid);
      timerid = setTimeout(function () {
        wait = false;
      }, delay);
    } else {
      if (timerid) clearTimeout(timerid);
      timerid = setTimeout(function () {
        fn.apply(void 0, _toConsumableArray(args));
      }, delay);
    }
  };
}
},{}],"src/throttle.js":[function(require,module,exports) {
"use strict"; // 节流

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = throttle;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function throttle(fn) {
  var interval = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
  var prev = Date.now();
  return function () {
    var args = arguments;
    var now = Date.now();
    var past = now - prev;

    if (interval < past) {
      fn.apply(void 0, _toConsumableArray(args));
      prev = Date.now();
    }
  };
}
},{}],"src/robotjs.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var RobotJs = /*#__PURE__*/function () {
  function RobotJs() {
    _classCallCheck(this, RobotJs);

    this.selectorType = 'normal';
    this.actions = [];
    this.params = {
      el: ''
    };
  }

  _createClass(RobotJs, [{
    key: "wait",
    value: function wait(selector) {
      var _this = this;

      var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
          _ref$interval = _ref.interval,
          interval = _ref$interval === void 0 ? 100 : _ref$interval,
          _ref$overtime = _ref.overtime,
          overtime = _ref$overtime === void 0 ? 3500 : _ref$overtime;

      var fn = function fn() {
        var time = 0;
        var el = '';

        if (selector[0] == '/') {
          _this.selectorType = 'xpath';
        } else {
          _this.selectorType = 'normal';
        }

        var repeater = setInterval(function () {
          if (_this.selectorType == 'xpath') {
            el = $x(selector);

            if (el.length != 0) {
              _this.params.el = el;

              _this.next();

              clearInterval(repeater);
            }
          }

          if (_this.selectorType == 'normal') {
            el = document.querySelectorAll(selector);

            if (el.length != 0) {
              _this.params.el = el;

              _this.next();

              clearInterval(repeater);
            }
          }

          time += interval;
        }, interval);
      };

      this.actions.push(fn);
      return this;
    }
  }, {
    key: "next",
    value: function next() {
      if (this.actions.length == 0) return;
      var ac = this.actions.shift();
      ac();
    }
  }, {
    key: "perform",
    value: function perform() {
      this.next();
    }
  }, {
    key: "click",
    value: function click() {
      var _this2 = this;

      var fn = function fn() {
        _this2.params.el.forEach(function (item) {
          item.click();
        });

        _this2.next();
      };

      this.actions.push(fn);
      return this;
    }
  }, {
    key: "send_keys",
    value: function send_keys(str) {
      var _this3 = this;

      var fn = function fn() {
        var letter = '';
        var strs = str.split(',');
        setInterval(function () {
          letter = strs.shift();
          _this3.params.el.val += letter;
        }, 250);
      };

      this.actions.push(fn);
      return this;
    }
  }]);

  return RobotJs;
}();

exports.default = RobotJs;
},{}],"src/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.phoneHide = phoneHide;
exports.cloneDeep = cloneDeep;
exports.objType = objType;
exports.unique = unique;
exports.strToNum = strToNum;
exports.getViewportOffset = getViewportOffset;
exports.quickSort = exports.arrItemCounts = exports.delArrItem = exports.arrFindItem = exports.bank_filter = exports.RobotJs = exports.throttle = exports.debounce = exports.platform = void 0;

var _platform = _interopRequireWildcard(require("./platform.js"));

var _debounce2 = _interopRequireDefault(require("./debounce.js"));

var _throttle2 = _interopRequireDefault(require("./throttle.js"));

var _robotjs2 = _interopRequireDefault(require("./robotjs.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var platform = _platform;
exports.platform = platform;
var debounce = _debounce2.default;
exports.debounce = debounce;
var throttle = _throttle2.default;
exports.throttle = throttle;
var RobotJs = _robotjs2.default; // 手机号码脱敏 前三后四 

exports.RobotJs = RobotJs;

function phoneHide(phone) {
  if (typeof phone == 'string') {
    //参数为字符串类型
    var newPhone = phone.substring(3, 8);
    return phone.replace(newPhone, '*****');
  }
} // 深拷贝


function cloneDeep(data) {
  var t = type(data),
      o,
      i,
      ni;

  if (t === 'array') {
    o = [];
  } else if (t === 'object') {
    o = {};
  } else {
    return data;
  }

  if (t === 'array') {
    for (i = 0, ni = data.length; i < ni; i++) {
      o.push(deepCopy(data[i]));
    }

    return o;
  } else if (t === 'object') {
    for (i in data) {
      o[i] = deepCopy(data[i]);
    }

    return o;
  }
}

function objType(obj) {
  var toString = Object.prototype.toString;
  var map = {
    '[object Boolean]': 'boolean',
    '[object Number]': 'number',
    '[object String]': 'string',
    '[object Function]': 'function',
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object RegExp]': 'regExp',
    '[object Undefined]': 'undefined',
    '[object Null]': 'null',
    '[object Object]': 'object'
  };
  return map[toString.call(obj)];
} // 数组去重


function unique(array) {
  var n = {},
      r = [],
      len = array.length,
      val,
      type;

  for (var i = 0; i < array.length; i++) {
    val = array[i];
    type = _typeof(val);

    if (!n[val]) {
      n[val] = [type];
      r.push(val);
    } else if (n[val].indexOf(type) < 0) {
      n[val].push(type);
      r.push(val);
    }
  }

  return r;
} // 字符串转数字，可以控制精度


function strToNum(str, num) {
  var num = num ? num : 0;
  var value = parseFloat(str);
  return parseFloat(value.toFixed(num));
} // 获取视口尺寸


function getViewportOffset() {
  if (window.innerWidth) {
    return {
      w: window.innerWidth,
      h: window.innerHeight
    };
  } else {
    // ie8及其以下
    if (document.compatMode === "BackCompat") {
      // 怪异模式
      return {
        w: document.body.clientWidth,
        h: document.body.clientHeight
      };
    } else {
      // 标准模式
      return {
        w: document.documentElement.clientWidth,
        h: document.documentElement.clientHeight
      };
    }
  }
} // 银行卡号分割


var bank_filter = function bank_filter(val) {
  val += '';
  val = val.replace(/(\s)/g, '').replace(/(\d{4})/g, '$1 ').replace(/\s*$/, '');
  return val;
}; // 查找数组中的元素


exports.bank_filter = bank_filter;

var arrFindItem = function arrFindItem(arr, num) {
  var index = -1;

  for (var i = 0; i < arr.length; i++) {
    if (num == arr[i]) {
      index = i;
      break;
    }
  }

  return index;
}; // 删除数组中的元素


exports.arrFindItem = arrFindItem;

var delArrItem = function delArrItem(arr, val) {
  var index = arrFindItem(arr, val);

  if (index != -1) {
    return arr.splice(index, 1);
  }
}; // 统计数组中各个元素出现的次数


exports.delArrItem = delArrItem;

var arrItemCounts = function arrItemCounts(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var m = arr[i];

    if (obj.hasOwnProperty(m)) {
      obj[m] += 1;
    } else {
      obj[m] = 1;
    }
  }

  return obj;
}; // 快速排序


exports.arrItemCounts = arrItemCounts;
var times = 0;

var quickSort = function quickSort(arr) {
  var j = arr.length - 1;
  var base = null;
  var tem = null;
  var bIndex = 0;
  var i = bIndex;

  do {
    base = arr[bIndex];

    do {
      while (j > i) {
        if (arr[j] < base) break;
        j--;
      }

      while (j > i) {
        if (arr[i] > base) break;
        i++;
      }

      if (i != j && j != bIndex) {
        tem = arr[i];
        arr[i] = arr[j];
        arr[j] = tem;
      } else if (j != bIndex) {
        tem = arr[i];
        arr[i] = arr[bIndex];
        arr[bIndex] = tem;
      } else {
        bIndex++;
      }
    } while (i != j);

    i = bIndex;
    j = arr.length - 1;
  } while (bIndex != j);
};

exports.quickSort = quickSort;
},{"./platform.js":"src/platform.js","./debounce.js":"src/debounce.js","./throttle.js":"src/throttle.js","./robotjs.js":"src/robotjs.js"}],"test/platform/index.js":[function(require,module,exports) {
"use strict";

var _index = require("../../src/index.js");

console.log(_index.platform.isChrome());
},{"../../src/index.js":"src/index.js"}],"../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "46269" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","test/platform/index.js"], null)
//# sourceMappingURL=/platform.565262db.js.map